package com.aswdc.metrimony;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.aswdc.metrimony.R;

public class dashboard extends AppCompatActivity {

    private Intent i1 = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        ImageView imageview1 = findViewById(R.id.imageview1);
        ImageView imageview2 = findViewById(R.id.imageview2);
        ImageView imageview3 = findViewById(R.id.imageview3);
        ImageView imageview4 = findViewById(R.id.imageview4);

        imageview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // button 1 was clicked!
                i1.setClass(getApplicationContext(),addcandidate.class);
                startActivity(i1);
            }
        });

        imageview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // button 2 was clicked!
                i1.setClass(getApplicationContext(),listcandidate.class);
                startActivity(i1);
            }
        });

        imageview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // button 2 was clicked!
                i1.setClass(getApplicationContext(),searchcandidate.class);
                startActivity(i1);
            }
        });

        imageview4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // button 1 was clicked!
                i1.setClass(getApplicationContext(),favoritecandidate.class);
                startActivity(i1);
            }
        });

        }
    }
