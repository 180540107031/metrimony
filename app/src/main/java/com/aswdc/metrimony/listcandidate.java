package com.aswdc.metrimony;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.aswdc.metrimony.R.*;

public class listcandidate extends AppCompatActivity {

    private Intent i1 = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_listcandidate);

        FloatingActionButton fab = (FloatingActionButton) findViewById(id.MyFab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // button 1 was clicked!
                i1.setClass(getApplicationContext(),addcandidate.class);
                startActivity(i1);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id=item.getItemId();

        i1.setClass(getApplicationContext(),favoritecandidate.class);
        startActivity(i1);
        return super.onOptionsItemSelected(item);
    }
}